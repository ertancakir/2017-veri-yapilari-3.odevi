#ifndef BINARY_TREE_HPP
#define BINARY_TREE_HPP

#include <cmath>
#include <string.h>
#include "ElemanYok.hpp"

struct Dugum{
	string veri;
	Dugum *sol;
	Dugum *sag;
	Dugum(const string& vr,Dugum *sl=NULL,Dugum *sg=NULL){
		veri=vr;
		sol=sl;
		sag=sg;
	}
};

class BinarySearchTree{
	private:
		Dugum *root;

		void AraveEkle(Dugum *&alt_Dugum, const string &yeni);
		bool AraveSil(Dugum *&alt_Dugum,const string &yeni);
		bool DugumSil(Dugum *&alt_Dugum);
		void Inorder(Dugum *alt_Dugum) const;
		public:
			BinarySearchTree();
			bool Bosmu() const;
			void Ekle(const string &yeni);
			void Sil(const string &veri) throw(ElemanYok);
			void Inorder() const;
			void Temizle();
			bool Ara(string ara);
			~BinarySearchTree();
};

#endif
