// �ablon ba��l liste
#ifndef BAGILLISTE_HPP
#define BAGILLISTE_HPP
#include <string.h>
#include "BinarySearchTree.hpp"
using namespace std;

struct Node{ //Düğüm yapısı
	string data;
	Node * next;
	BinarySearchTree * tree;
	Node(string data = string(), Node * next = NULL, BinarySearchTree * tree = NULL){
		this->data = data;
		this->next = next;
		this->tree = tree;
	}
};

class LinkedList{ //BağlıListe Sınıfı
	private:
		Node * root; //Listeyi gösteren baş düğüm
		int size = 0;

	public:
		LinkedList();
		Node * firstNode();
		int getSize();
		void add(const string& newData);
		void add(const string& newData, string * treeData);
		void addTree(int location, BinarySearchTree * newTree);
		const string& get(int location);
		int Ara(string arananKelime);
		void Listele();
		~LinkedList();
};
#endif
