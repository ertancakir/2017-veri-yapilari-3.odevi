#ifndef ISLEM_HPP
#define ISLEM_HPP

#include <dirent.h>
#include <string>
#include <fstream>
#include "BagilListe.hpp"
using namespace std;

class Islem{

  private:
    DIR *dir;
    struct dirent * d;
    LinkedList * liste;

  public:
    Islem();
    void dosyaBul(char * path);
    void agacEkle(char * path);
    void Ara(string arananKelime);
		void Listele();
    ~Islem();

};
#endif
