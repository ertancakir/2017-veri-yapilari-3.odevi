#include <dirent.h>
#include <iostream>
#include <string>
#include <dirent.h>
#include "BinarySearchTree.hpp"
#include "BagilListe.hpp"
#include "Islem.hpp"

using namespace std;

int main(void)
{
  int result = 0;
  char path[80];
  cout<<"Dosya yolunu giriniz : ";
  cin>>path;

  Islem * islem = new Islem();
  islem->dosyaBul(path);
  do{
    cout<<"1. Arama Yap"<<endl;
    cout<<"2. Listele"<<endl;
    cout<<"3. Cikis"<<endl;
    cout<<"Secim : ";
    cin>>result;

    if(result == 1){
      string aKelime;
      cout<<"Aranacak kelime : ";
      cin>>aKelime;
      islem->Ara(aKelime);
    }

    else if(result == 2){
      islem->Listele();
    }
  }while(result != 3);

  delete islem;
  return 0;
}
