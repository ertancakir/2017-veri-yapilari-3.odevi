#include "BinarySearchTree.hpp"



void BinarySearchTree::AraveEkle(Dugum *&alt_Dugum, const string &yeni){
  if(alt_Dugum == NULL) alt_Dugum = new Dugum(yeni);
  else if(yeni < alt_Dugum->veri)
    AraveEkle(alt_Dugum->sol,yeni);
  else if(yeni > alt_Dugum->veri)
    AraveEkle(alt_Dugum->sag,yeni);
  else return; // Ayn� eleman var.
}

bool BinarySearchTree::AraveSil(Dugum *&alt_Dugum,const string &yeni)
{
  if(alt_Dugum == NULL) return false; //Eleman yok
  if(alt_Dugum->veri == yeni)
    return DugumSil(alt_Dugum);
  else if(yeni < alt_Dugum->veri)
    return AraveSil(alt_Dugum->sol,yeni);
  else
    return AraveSil(alt_Dugum->sag,yeni);
}

bool BinarySearchTree::DugumSil(Dugum *&alt_Dugum)
{
  Dugum *silinecekDugum = alt_Dugum;

  if(alt_Dugum->sag == NULL) alt_Dugum = alt_Dugum->sol;
  else if(alt_Dugum->sol == NULL)alt_Dugum = alt_Dugum->sag;
  else{
    silinecekDugum = alt_Dugum->sol;
    Dugum *ebeveynDugum = alt_Dugum;
    while(silinecekDugum->sag != NULL){
      ebeveynDugum=silinecekDugum;
      silinecekDugum=silinecekDugum->sag;
    }
    alt_Dugum->veri = silinecekDugum->veri;
    if(ebeveynDugum == alt_Dugum) alt_Dugum->sol = silinecekDugum->sol;
    else ebeveynDugum->sag = silinecekDugum->sol;
  }
  delete silinecekDugum;
  return true;
}

void BinarySearchTree::Inorder(Dugum *alt_Dugum) const{
  if(alt_Dugum != NULL){
    Inorder(alt_Dugum->sol);
    cout<<alt_Dugum->veri<<" ";
    Inorder(alt_Dugum->sag);
  }
}

BinarySearchTree::BinarySearchTree(){
  root = NULL;
}

bool BinarySearchTree::Bosmu() const{
  return root == NULL;
}
void BinarySearchTree::Ekle(const string &yeni)
{
  AraveEkle(root,yeni);
}
void BinarySearchTree::Sil(const string &veri) throw(ElemanYok)
{
  if(AraveSil(root,veri) == false) throw ElemanYok();
}
void BinarySearchTree::Inorder() const{
  Inorder(root);
}
void BinarySearchTree::Temizle(){
  while(!Bosmu()) DugumSil(root);
}
bool BinarySearchTree::Ara(string ara){
  Dugum *temp=root;      //'head' is pointer to root node
  while(temp!=NULL)
  {
     if(temp->veri==ara)
         break;

     else if(ara>temp->veri)
         temp=temp->sag;

     else if(ara<temp->veri)
         temp=temp->sol;
 }
 if(temp==NULL)
    return false;
  return true;
}
BinarySearchTree::~BinarySearchTree(){
  Temizle();
}
