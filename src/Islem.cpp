#include "Islem.hpp"


Islem::Islem(){
  liste = new LinkedList();
}
void Islem::dosyaBul(char * path){
  dir = opendir(path);
  while(d = readdir(dir)){
    string dosyaAdi = d->d_name;
    if(dosyaAdi.find(".txt",0)<100){
      liste->add(dosyaAdi);
    }
  }
  closedir(dir);
  agacEkle(path);
}

void Islem::agacEkle(char * path){
  string sPath = path;
  string tmp;
  for(int i=1;i<=liste->getSize();i++){
    BinarySearchTree * tmpTree = new BinarySearchTree();
    ifstream in(sPath + "//" + liste->get(i));
    while(!in.eof()){
      in>>tmp;
      tmpTree->Ekle(tmp);
    }
    liste->addTree(i,tmpTree);
    in.close();
  }
}

void Islem::Ara(string arananKelime){
  int index = liste->Ara(arananKelime);
  if(index != 0){
    cout<<"Kelime "<<liste->get(index)<<" icinde"<<endl;
  }
}


void Islem::Listele(){
  liste->Listele();
}

Islem::~Islem(){
  delete liste;
}
