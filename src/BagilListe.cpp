#include "BagilListe.hpp"


LinkedList::LinkedList(){
  root = new Node();
}

Node * LinkedList::firstNode(){ //Baş düğümün gösterdiğini düğümü döndürür
  return root->next;
}

int LinkedList::getSize(){
  return size;
}

void LinkedList::add(const string& newData){
  Node * tmp = root;
  while(tmp->next != NULL){
    tmp = tmp->next;
  }
  tmp->next = new Node(newData,NULL,NULL);
  size++;
}

void LinkedList::add(const string& newData, string * treeData){ //string değerini listeye ekler
  Node * tmp = root;
  while(tmp->next != NULL){
    tmp = tmp->next;
  }
  BinarySearchTree *	newTree = new BinarySearchTree();
  for(int i=0;i<=sizeof(treeData);i++){
    newTree->Ekle(treeData[i]);
  }
  tmp->next = new Node(newData,NULL,newTree);
}

void LinkedList::addTree(int location, BinarySearchTree * newTree){
  Node * tmp = root;
  for(int i = 1;i<=location;i++){
    tmp = tmp->next;
  }
  tmp->tree = newTree;
}

const string& LinkedList::get(int location){ //Belirtilen konumdaki düğümün verisini döndürür
  int index = 0;
  Node * tmp = root;
  while(tmp->next != NULL && location!=index++){
    tmp = tmp->next;
  }
  return tmp->data;
}

int LinkedList::Ara(string arananKelime){
  Node * tmp = root;
  for(int i=1;i<=size;i++){
    if(tmp->next != NULL) tmp = tmp->next;
    if(tmp->tree->Ara(arananKelime)){
      return i;
    }
  }
  return 0;
}

void LinkedList::Listele(){
  Node * tmp = root;
  for(int i=1;i<=this->getSize();i++){
    if(tmp->next != NULL) tmp = tmp->next;
    cout<<tmp->data<<":"<<endl;
    tmp->tree->Inorder();
    cout<<endl;
  }
}

LinkedList::~LinkedList(){
  Node * tmp = root;
  do{
    Node * silinecekDugum = tmp->next;
    tmp->next = tmp->next->next;
    delete silinecekDugum->tree;
    delete silinecekDugum;
  }while(tmp->next != NULL);
  delete root;
}
